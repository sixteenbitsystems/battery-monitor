# Sixteenbit Battery Monitor

#### Automated Software Install

Go to raspberry command prompt or SSH.
Make sure you are in the home directory by typing ```cd ~ ``` and then type:

```bash
wget https://raw.githubusercontent.com/sixteenbit/Mintybatterymonitor/master/InstallBatteryMonitor.sh
```

Then type:
```bash
sudo git clone https://gitlab.com/sixteenbitsystems/battery-monitor.git
```

Then type:
```bash
sudo chmod 777 InstallBatteryMonitor.sh
```

And then type:

```bash
sudo ./InstallBatteryMonitor.sh
```

Finally reboot to have it all start on boot with:

```bash
sudo reboot
```
